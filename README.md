### homebrew install
```bash
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew update
```

### setting JAVA_HOME
```bash
brew tap caskroom/versions
brew cask install java
brew cask install java8
vim ~/.bash_profile
--- add
export JAVA_HOME=`/usr/libexec/java_home -v 1.8`

alias la='ls -la'
alias ll='ls -la'
alias vi='vim'
---
source ~/.bash_profile
```

### application install
```bash
brew install git
brew tap caskroom/cask
git clone https://nozomitaguchi@bitbucket.org/nozomitaguchi/pc_setting.git
cd pc_setting/
brew bundle
```
